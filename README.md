puzzle:
https://adventofcode.com/2022/day/1

solution example:
https://onecompiler.com/python/3zszwma9r

run:
cat ..\input\day1\input.txt | python .\day01_simple.py
