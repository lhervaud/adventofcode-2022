# Find the item type that appears in both compartments of each rucksack. What is the sum of the priorities of those item types?
from sys import stdin
from pandas import DataFrame as df
import numpy as np

def get_letter_value(c):
    if (c >= 'a' and c <= 'z'):
        return ord(c)-96
    elif (c >= 'A' and c <= 'Z'):
        return ord(c)-38
    else:
        return 0

rows = []
inters = []

for line in stdin:
    input = list(line.rstrip())
    half = int(len(input)/2)
    rows.append([[input[:half]] , [input[half:]]])
    inters.append([np.intersect1d([input[:half]], input[half:])[0], get_letter_value(np.intersect1d([input[:half]], input[half:])[0])])

print(inters)

df_score = df(inters)

print(df_score[1].sum())

#Lowercase item types a through z have priorities 1 through 26.
#Uppercase item types A through Z have priorities 27 through 52.

