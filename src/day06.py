# How many characters need to be processed before the first start-of-packet marker is detected?

from sys import stdin

PART_LEN = 14

def main():

    for line in stdin:
        input = line.rstrip()
        for i in range(len(input)-PART_LEN):
            s = set(x for x in input[i:i+PART_LEN])
            #print(s)
            if len(s) == PART_LEN:
                print('result:'+str(i+PART_LEN))
                break

if __name__ == "__main__":
    main()
