# What is the level of monkey business after 20 rounds of stuff-slinging simian shenanigans?

# PART 2
# what is the level of monkey business after 10000 rounds?

from sys import stdin
import re

ROUND = 1000
PART = 2

def main():

    monkeys = []
    monkey = Monkey("")
    # read input
    for line in stdin:
        input = line.rstrip()
        if re.match(r"Monkey (\d):", input):
            result = re.search(r"Monkey (\d):", input)
            name = result.group(1)
            monkey = Monkey(name)
        elif re.match(r"  Starting items: (.+)", input):
            result = re.search(r"  Starting items: (.+)", input)
            items = result.group(1).split(", ")
            monkey.items = items
        elif re.match(r"  Operation: new = old (.) (.+)", input):
            result = re.search(r"  Operation: new = old (.) (.+)", input)
            items = result.group(1)
            monkey.operator = result.group(1)
            monkey.ope_value = result.group(2)
        elif re.match(r"  Test: divisible by (\d+)", input):
            result = re.search(r"  Test: divisible by (\d+)", input)
            monkey.divisible = int(result.group(1))
        elif re.match(r"    If true: throw to monkey (\d+)", input):
            result = re.search(r"    If true: throw to monkey (\d+)", input)
            monkey.div_true = int(result.group(1))
        elif re.match(r"    If false: throw to monkey (\d+)", input):
            result = re.search(r"    If false: throw to monkey (\d+)", input)
            monkey.div_false = int(result.group(1))
            monkeys.append(monkey)

    run(monkeys)


def run(monkeys):

    for round in range(ROUND):
        #print(round)
        for m in monkeys:
            #print(m.name, m.items)
            for i in m.items:
                (new_worry,new_monkey) = m.get_new_worry(int(i))
                if new_worry > 1000:
                    new_worry = new_worry/10
                monkeys[new_monkey].items.append(new_worry)
            m.inspected_items += len(m.items)
            m.items = []

    for x in monkeys:
        print(x.name, x.inspected_items)

    # results
    results = []
    for m in monkeys:
        results.append(m.inspected_items)
    results.sort(reverse = True)
    print(results[0] * results[1])


class Monkey:

    def __init__(self, name):
        self.name = name
        self.items = []
        self.operator = ""
        self.ope_value = ""
        self.divisible = 0
        self.div_true = 0
        self.div_false = 0
        self.inspected_items = 0

    def get_new_worry(self, worry_level):

        new_worry = 0
        new_monkey = 0

        if self.ope_value == "old":
            ope_value = worry_level
        else:
            ope_value = int(self.ope_value)
        
        if self.operator == "*":
            new_worry = worry_level * ope_value
        else:
            new_worry = worry_level + ope_value

        if PART == 1:
            new_worry = int(new_worry/3)
        #else:
        #    if new_worry > 1000:
        #        new_worry = int(new_worry/10)

        if new_worry % self.divisible == 0:
            new_monkey = self.div_true
        else:
            new_monkey = self.div_false

        return(new_worry,new_monkey)

    
if __name__ == "__main__":
    main()
