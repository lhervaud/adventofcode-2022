# After the rearrangement procedure completes, what crate ends up on top of each stack?

from sys import stdin
import re

PART = 2

def main():

    stacks = []
    init = True
    start_move = False

    for line in stdin:
        input = line.rstrip()
        #print(input)
        if init:
            nb_stack = int((len(line)+1) / 4)
            for i in range(nb_stack):
                stacks.append(list())
            init = False

        if start_move == False:
            if not input.startswith(" 1"):
                for i in range(nb_stack):
                    cave = line[(i*4)+1]
                    if cave != ' ':
                        stacks[i].append(cave)
                    #print(cave)
            else:
                for i in range(nb_stack):
                    stacks[i].reverse()
                start_move = True
                #print(stacks)
        else:
            if input:
                result = re.search(r"move (\d+) from (\d+) to (\d+)", input)
                nb_move = int(result.group(1))
                i_out = int(result.group(2)) - 1
                i_in = int(result.group(3)) - 1
                if PART == 1:
                    for i in range(nb_move):
                        ele = stacks[i_out].pop()
                        stacks[i_in].append(ele)
                        #print(stacks)
                else:
                    list_tmp = []
                    for i in range(nb_move):
                        ele = stacks[i_out].pop()
                        list_tmp.append(ele)
                    list_tmp.reverse()
                    stacks[i_in].extend(list_tmp)

        
    print(stacks)
    output = ""
    for i in range(nb_stack):
        output += stacks[i][-1:][0]
    print(output)

if __name__ == "__main__":
    main()
