# How many positions does the tail of the rope visit at least once?

# PART2

from sys import stdin
import re

PART = 1

def main():

    motions = []
    right = 0
    left = 0
    up = 0
    down = 0

    # init motions
    for line in stdin:
        input = line.rstrip().split(' ')
        direction = input[0]
        moves = int(input[1])
        if direction == 'R':
            right += moves
        elif direction == 'L':
            left += moves
        elif direction == 'U':
            up += moves
        elif direction == 'D':
            down += moves
        motions.append((direction, moves))

    #print(motions)

    # init map
    map = []
    max_row = max(right, left) * 2
    max_col = max(up, down) * 2

    for i in range(max_col):
        map.append([0] * max_row)

    #for x in map:
    #    print(x)
    
    run(motions, map, max_row, max_col)


def run(motions, map, max_row, max_col):

    # start center
    H_row = int(max_row/2)
    H_col = int(max_col/2)
    S_row = H_row
    S_col = H_col
    print(S_row,S_col)
    map[S_col][S_row] = 1

    # move
    for motion in motions:
        print(motion)
        if motion[0] == 'R':
            for i in range(motion[1]):
                H_row += 1
                if H_row - S_row > 1:
                    S_row += 1
                    S_col = H_col
                map[S_col][S_row] = 1
        if motion[0] == 'L':
            for i in range(motion[1]):
                H_row -= 1
                if S_row - H_row > 1:
                    S_row -= 1
                    S_col = H_col
                map[S_col][S_row] = 1
        if motion[0] == 'U':
            for i in range(motion[1]):
                H_col -= 1
                if S_col - H_col > 1:
                    S_col -= 1
                    S_row = H_row
                map[S_col][S_row] = 1
        if motion[0] == 'D':
            for i in range(motion[1]):
                H_col += 1
                if H_col - S_col > 1:
                    S_col += 1
                    S_row = H_row
                map[S_col][S_row] = 1

    #for x in map:
    #    print(x)

    nb_positions = 0
    for x in range(max_col):
        for y in range(max_row):
            if map[x][y] == 1:
                nb_positions += 1
    
    print(nb_positions)

if __name__ == "__main__":
    main()
