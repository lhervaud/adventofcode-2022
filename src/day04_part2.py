# In how many assignment pairs do the ranges overlap?

from sys import stdin
import numpy as np

def main():

    overlap_count = 0

    for line in stdin:
        input = line.rstrip()
        #print(input)
        pair = input.split(',')
        sr1 = pair[0].split('-')
        r1 = range(int(sr1[0]), int(sr1[1])+1)
        sr2 = pair[1].split('-')
        r2 = range(int(sr2[0]), int(sr2[1])+1)
        inter = np.intersect1d(r1, r2)
        #print(inter)
        if len(inter) > 0:
            overlap_count += 1

    print(overlap_count)

if __name__ == "__main__":
    main()
