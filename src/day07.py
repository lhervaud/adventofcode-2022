# Find all of the directories with a total size of at most 100000.
# What is the sum of the total sizes of those directories?

# PART2
# Find the smallest directory that, if deleted, would free up enough space on the filesystem to run the update. What is the total size of that directory?

from sys import stdin
import re

PART = 2
DISK = 70000000
MIN_PART2 = 30000000

def main():

    rootdir = Dir("/", None)
    currentdir = None

    # init
    for line in stdin:
        input = line.rstrip()
        #print(input)
        if re.match(r"\$ cd (.+)", input):
            result = re.search(r"\$ cd (.+)", input)
            dirname = result.group(1)
            if dirname == "/":
                currentdir = rootdir
            elif dirname == "..":
                currentdir = currentdir.levelup
            else:
                d = Dir(dirname, currentdir)
                currentdir.add_subdir(d)
                currentdir = d
        elif input.startswith("dir "):
            dirname = input.split(' ')[1]
            currentdir.add_subdir(Dir(dirname, currentdir))
        elif re.match(r"\d+ ", input):
            result = re.search(r"(\d+) (.+)", input)
            filesize = int(result.group(1))
            filename = result.group(2)
            f = File(filename, filesize)
            currentdir.add_file(f)


    #print(rootdir.get_size())
    all_dir = rootdir.get_all_dir()
    if PART == 1:
        total_size = 0
        for dir in all_dir:
            size = dir[1].get_size()
            if size <= 100000:
                total_size += size
        print(total_size)
    else:
        space_used = rootdir.get_size()
        need_space = DISK - space_used
        required_space = MIN_PART2 - need_space
        min_size = space_used
        for dir in all_dir:
            size = dir[1].get_size()
            if size >= required_space and size <= min_size:
                min_size = size
        print(min_size)


class File:

    def __init__(self, name, size):
        self.name = name
        self.size = size

class Dir:

    def __init__(self, name, levelup):
        self.name = name
        self.files = []
        self.subdirs = {}
        self.levelup = levelup

    def add_file(self, file):
        self.files.append(file)

    def add_subdir(self, dir):
        self.subdirs[dir.name] = dir

    def subdir(self, dirname):
        return self.subdirs[dirname]

    def get_size(self):
        dir_size = 0
        for file in self.files:
            dir_size += file.size
        for key in self.subdirs:
            dir_size += self.subdirs[key].get_size()
        return dir_size

    def get_all_dir(self):
        all_dir = list(self.subdirs.items())
        for key in self.subdirs:
            all_dir.extend(self.subdirs[key].get_all_dir())
        return all_dir


if __name__ == "__main__":
    main()
