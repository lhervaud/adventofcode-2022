from sys import stdin

most_calories = 0

current_elf_calories = 0
for line in stdin:
    if line.rstrip() == '':
        # change elf
        if current_elf_calories > most_calories:
            # keep most calories is needed
            most_calories = current_elf_calories
        # reset current total
        current_elf_calories = 0
    else:
        # add calories to total
        current_elf_calories += int(line.rstrip())

# display most calories total
print(most_calories)