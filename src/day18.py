# What is the surface area of your scanned lava droplet?

# PART 2
# 

from sys import stdin

PART = 1

def main():

    cubes = []

    # init puzzle
    for line in stdin:
        input = line.rstrip()
        tuple = input.split(",")
        cubes.append((int(tuple[0]),int(tuple[1]),int(tuple[2])))

    #print(cubes)

    print("surface area:",run(cubes))

def run(cubes):

    total_surf = 0
    for i in range(len(cubes)):
        cube1 = cubes[i]
        surface = 6
        for j in range(i+1, len(cubes)):
            cube2 = cubes[j]
            for e in range(3):
                if e==0:
                    if abs(cube1[0]-cube2[0]) == 1 and cube1[1] == cube2[1] and cube1[2] == cube2[2]:
                        surface -= 2
                elif e==1:
                    if cube1[0] == cube2[0] and abs(cube1[1]-cube2[1]) == 1 and cube1[2] == cube2[2]:
                        surface -= 2
                elif e==2:
                    if cube1[0] == cube2[0] and cube1[1] == cube2[1] and abs(cube1[2]-cube2[2]) == 1:
                        surface -= 2
        total_surf += surface

    return(total_surf)


if __name__ == "__main__":
    main()
