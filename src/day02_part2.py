from sys import stdin
from pandas import DataFrame as df

score = {}
transco = {}
# A for Rock, B for Paper, and C for Scissors
# X means you need to lose, Y means you need to end the round in a draw, and Z means you need to win
# X for Rock, Y for Paper, and Z for Scissors
# 1 for Rock, 2 for Paper, and 3 for Scissors
# 0 if you lost, 3 if the round was a draw, and 6 if you won

score["A X"] = 1 + 3
score["A Y"] = 2 + 6
score["A Z"] = 3 + 0
score["B X"] = 1 + 0
score["B Y"] = 2 + 3
score["B Z"] = 3 + 6
score["C X"] = 1 + 6
score["C Y"] = 2 + 0
score["C Z"] = 3 + 3

transco["A X"] = "A Z"
transco["A Y"] = "A X"
transco["A Z"] = "A Y"
transco["B X"] = "B X"
transco["B Y"] = "B Y"
transco["B Z"] = "B Z"
transco["C X"] = "C Y"
transco["C Y"] = "C Z"
transco["C Z"] = "C X"

rows = []

for line in stdin:
    combi = line.rstrip()
    if combi in transco:
        rows.append([combi, score[transco[combi]]])
    else:
        print("key not exist:" + combi)

#print(rows)

df_score = df(rows)
#print(df_score)
print(df_score[1].sum())
