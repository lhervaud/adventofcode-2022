# Find the signal strength during the 20th, 60th, 100th, 140th, 180th, and 220th cycles.
# What is the sum of these six signal strengths?

# PART 2
# What eight capital letters appear on your CRT?

from sys import stdin
import re

PART = 2

def main():

    if PART == 1:
        run1()
    else:
        run2()

def run2():

    cycle = 0
    col = 0
    row = 0
    sprite_min = 0
    sprite_max = 2

    map = []
    for i in range(6):
        map.append([" "] * 40)
    
    # read input
    for line in stdin:
        #print(line, cycle, col, row)
        input = line.rstrip().split(' ')
        if input[0] == "noop":
            if col >= sprite_min and col <= sprite_max:
                map[row][col] = "#"
            else:
                map[row][col] = "."
            cycle += 1
            col += 1
            if col > 39:
                row += 1
                col = 0
        elif input[0] == "addx":
            value = int(input[1])
            if col >= sprite_min and col <= sprite_max:
                map[row][col] = "#"
            else:
                map[row][col] = "."
            cycle += 1
            col += 1
            if col > 39:
                row += 1
                col = 0
            if col >= sprite_min and col <= sprite_max:
                map[row][col] = "#"
            else:
                map[row][col] = "."
            cycle += 1
            col += 1
            if col > 39:
                row += 1
                col = 0
            sprite_min += value
            sprite_max += value
            
    for x in map:
        print("".join(x))

def run1():
    X = 1
    cycle = 0
    strengths = 0

    # read input
    for line in stdin:
        #print(line, X)
        input = line.rstrip().split(' ')
        if input[0] == "noop":
            cycle += 1
            strengths += check_cycle(cycle, X)
        elif input[0] == "addx":
            value = int(input[1])
            cycle += 1
            strengths += check_cycle(cycle, X)
            cycle += 1
            strengths += check_cycle(cycle, X)
            X += value

    print(strengths)

def check_cycle(cycle, X):

    if cycle == 20 or cycle == 60 or cycle == 100 or cycle == 140 or cycle == 180 or cycle == 220:
        #print(cycle, X)
        return(cycle * X)
    else:
        return(0)


if __name__ == "__main__":
    main()
