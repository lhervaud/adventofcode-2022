# how many trees are visible from outside the grid?

# PART2
# What is the highest scenic score possible for any tree?

from sys import stdin
import re

PART = 2

def main():

    map = []

    # init map
    for line in stdin:
        input = line.rstrip()
        line_map = []
        for c in input:
            line_map.append(int(c))
        map.append(line_map)

    #print(map)
    if PART == 1:
        part1(map)
    else:
        part2(map)


def part1(map):
    # compute
    width = len(map[0])
    height = len(map)
    # edge
    edge = (width*2) + ((height-2)*2)
    print(edge)
    # interior
    interior = 0
    for r in range(1,width-1):
        for c in range(1, height-1):
            current = map[r][c]
            #print(current)
            visible = False
            # up
            shorter = True
            for x in range(r-1, -1, -1):
                #print("up", map[x][c])
                if map[x][c] >= current:
                    shorter = False
                #print('shorter', shorter)
            if shorter == True:
                visible = True
            # down
            shorter = True
            for x in range(r+1, width, 1):
                #print("down", map[x][c])
                if map[x][c] >= current:
                    shorter = False
                #print('shorter', shorter)
            if shorter == True:
                visible = True
            # left
            shorter = True
            for y in range(c-1, -1, -1):
                #print("left", map[r][y])
                if map[r][y] >= current:
                    shorter = False
                #print('shorter', shorter)
            if shorter == True:
                visible = True
            # right
            shorter = True
            for y in range(c+1, height, 1):
                #print("right", map[r][y])
                if map[r][y] >= current:
                    shorter = False
                #print('shorter', shorter)
            if shorter == True:
                visible = True
            #print(visible)
            if visible == True:
                interior += 1
    print(interior)
    print(edge+interior)


def part2(map):

    # compute
    width = len(map[0])
    height = len(map)

    # interior
    scenic_score = 0
    for r in range(1,width-1):
        for c in range(1, height-1):
            current = map[r][c]
            #print(current)
            current_score = 1
            # up
            up_score = 1
            for x in range(r-1, 0, -1):
                #print("up", map[x][c])
                if map[x][c] < current:
                    up_score += 1
                else:
                    break
            # down
            down_score = 1
            for x in range(r+1, width-1, 1):
                #print("down", map[x][c])
                if map[x][c] < current:
                    down_score += 1
                else:
                    break
            # left
            left_score = 1
            for y in range(c-1, 0, -1):
                #print("left", map[r][y])
                if map[r][y] < current:
                    left_score += 1
                else:
                    break
            # right
            right_score = 1
            for y in range(c+1, height-1, 1):
                #print("right", map[r][y])
                if map[r][y] < current:
                    right_score += 1
                else:
                    break
            #print("current_score",up_score , down_score , left_score , right_score)
            current_score = up_score * down_score * left_score * right_score
            if current_score > scenic_score:
                scenic_score = current_score

    print(scenic_score)


if __name__ == "__main__":
    main()
