import sys
import pandas as pd

elf = 1
rows = []

for line in sys.stdin:
    if not line.rstrip():
        elf += 1
    else:
        rows.append([elf, int(line.rstrip())])

#print(rows)

df = pd.DataFrame(rows)
#print(df)
print(df.groupby([0]).sum().sort_values(by=1, axis=0, ascending=False))