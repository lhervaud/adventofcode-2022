# In the row where y=2000000, how many positions cannot contain a beacon?

# PART 2
# 

from sys import stdin
import re
import copy

PART = 1
#Y_COUNT=10
Y_COUNT=2000000

VOID = '.'
SENSOR = 'S'
BEACON = 'B'
BLOCK = '#'

def main():

    sensors = []
    beacons = []
    map = []
    x_min = 0
    x_max = 0
    y_min = 0
    y_max = 0
    max_distance = 0

    # init puzzle
    for line in stdin:
        input = line.rstrip()
        if input:
            result = re.search(r"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)", input)
            x_sensor = int(result.group(1))
            y_sensor = int(result.group(2))
            x_beacon = int(result.group(3))
            y_beacon = int(result.group(4))
            # optimization: remove is distance not concerned by Y_COUNT
            distance = abs(x_sensor-x_beacon) + abs(y_sensor-y_beacon)
            print("distance:", distance)
            #if (y_sensor<Y_COUNT and y_sensor+distance<Y_COUNT) or (y_sensor>Y_COUNT and y_beacon-distance>Y_COUNT):
                #print("exclude for optimization:",input)
            #    pass
            #else:
            max_distance = max(max_distance, distance)
            print("keep:", input, max_distance)
            sensors.append((x_sensor,y_sensor))
            beacons.append((x_beacon,y_beacon))

    x_min = min(min([x[0] for x in sensors]), min([x[0] for x in beacons])) - max_distance
    x_max = max(max([x[0] for x in sensors]), max([x[0] for x in beacons])) + max_distance
    y_min = min(min([x[1] for x in sensors]), min([x[1] for x in beacons]))
    y_max = max(max([x[1] for x in sensors]), max([x[1] for x in beacons]))
    print(x_min, x_max, y_min, y_max)

    # init map
    #for i in range(y_min, y_max+1):        
    #    map.append(list(VOID * (x_max-x_min+1)))
    line_map = list(VOID * (x_max-x_min+1))
    print('len(line_map)',len(line_map))

    # build map according to puzzle input
    for sensor in sensors:
        x = sensor[0] - x_min
        y = sensor[1] - y_min
        if y == Y_COUNT-y_min:
            line_map[x] = SENSOR
    for beacon in beacons:
        x = beacon[0] - x_min
        y = beacon[1] - y_min
        if y == Y_COUNT-y_min:
            line_map[x] = BEACON

    #print_map(map, x_min, x_max, y_min, y_max)
    print("positions:",run(line_map, sensors, beacons, x_min, x_max, y_min, y_max))
    #print(line_map)

    #print_map(map, x_min, x_max, y_min, y_max)

def run(line_map, sensors, beacons, x_min, x_max, y_min, y_max):

    print('len(sensors):', len(sensors))
    for i in range(len(sensors)):
    #for i in range(1):
        #map = f = copy.deepcopy(map_in)
        sensor = sensors[i]
        beacon = beacons[i]
        x_sensor = sensor[0] - x_min
        y_sensor = sensor[1] - y_min
        x_beacon = beacon[0] - x_min
        y_beacon = beacon[1] - y_min
        # Manhattan distance
        d = 0
        y = (Y_COUNT-y_min)
        distance = abs(x_sensor-x_beacon) + abs(y_sensor-y_beacon)
        diff = abs(y - y_sensor)
        x_min_count = x_sensor - distance + diff
        x_max_count = x_sensor + distance - diff
        print(x_min_count,x_max_count)
        for x in range(x_min_count, x_max_count + 1):
            if line_map[x] == VOID:            
                line_map[x] = BLOCK

    #print(map[Y_COUNT-y_min])
    return(len(list(filter(lambda value: value == BLOCK, list(line_map)))))


def run_pas_perf(line_map, sensors, beacons, x_min, x_max, y_min, y_max):

    print('len(sensors):', len(sensors))
    for i in range(len(sensors)):
    #for i in range(1):
        #map = f = copy.deepcopy(map_in)
        sensor = sensors[i]
        beacon = beacons[i]
        beacon_found = False
        x_sensor = sensor[0] - x_min
        y_sensor = sensor[1] - y_min
        x_beacon = beacon[0] - x_min
        y_beacon = beacon[1] - y_min
        # Manhattan distance
        d = 0
        while beacon_found == False:
            d += 1
            if d % 10000 == 0:
                print(d)
            # up left
            x = x_sensor
            y = y_sensor - d
            for i in range(1, d+1):
                #print("up left d:",d)
                if x == x_beacon and y == y_beacon:
                    beacon_found = True
                if y == (Y_COUNT-y_min):
                    line_map[x] = BLOCK
                    # if x>=0 and x<=(x_max-x_min) and y>=0 and y<len(map):
                    #     if map[y][x] == VOID:
                    #         map[y][x] = BLOCK
                x -= 1
                y += 1
            # down left
            x = x_sensor - d
            y = y_sensor
            for i in range(1, d+1):
                #print("down left d:",d)
                if x == x_beacon and y == y_beacon:
                    beacon_found = True
                if y == (Y_COUNT-y_min):
                    line_map[x] = BLOCK
                    # if x>=0 and x<=(x_max-x_min) and y>=0 and y<len(map):
                    #     if map[y][x] == VOID:
                    #         map[y][x] = BLOCK
                x += 1
                y += 1
            # down right
            x = x_sensor
            y = y_sensor + d
            for i in range(1, d+1):
                #print("down right d:",d)
                if x == x_beacon and y == y_beacon:
                    beacon_found = True
                if y == (Y_COUNT-y_min):
                    line_map[x] = BLOCK
                    # if x>=0 and x<=(x_max-x_min) and y>=0 and y<len(map):
                    #     if map[y][x] == VOID:
                    #         map[y][x] = BLOCK
                x += 1
                y -= 1
            # up right
            x = x_sensor + d
            y = y_sensor
            for i in range(1, d+1):
                #print("up right d:",d)
                if x == x_beacon and y == y_beacon:
                    beacon_found = True
                if y == (Y_COUNT-y_min):
                    line_map[x] = BLOCK
                    # if x>=0 and x<=(x_max-x_min) and y>=0 and y<len(map):
                    #     if map[y][x] == VOID:
                    #         map[y][x] = BLOCK
                x -= 1
                y -= 1

            #if d == 10:
            #    beacon_found = True

        #print_map(map, x_min, x_max, y_min, y_max)

    #print(map[Y_COUNT-y_min])
    return(len(list(filter(lambda value: value == BLOCK, list(line_map)))))


def print_map(map, x_min, x_max, y_min, y_max):
    print(x_min)
    number = y_min
    for line_map in map:
        print("%8d " % (number,), end = '')
        number += 1
        print(''.join(line_map))

if __name__ == "__main__":
    main()
