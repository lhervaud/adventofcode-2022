# What is the fewest steps required to move from your current position to the location that should get the best signal?
# Sabqponm
# abcryxxl
# accszExk
# acctuvwj
# abdefghi

# PART 2
# What is the fewest steps required to move starting from any square with elevation a to the location that should get the best signal?

from sys import stdin

PART = 2
START = ord('S')
END = ord('E')
PASSED = 1000

def main():

    heightmap = []

    # init map
    for line in stdin:
        input = line.rstrip()
        line_map = []
        for c in input:
            line_map.append(ord(c))
        heightmap.append(line_map)

    for l in heightmap:
        print(l)

    run(heightmap)


def run(map):

    global END

    width = len(map[0])
    height = len(map)

    # retreive start
    x = 0
    y = 0
    for r in range(0,height):
        for c in range(0, width):
            if map[r][c] == START:
                x = c
                y = r
                print('Start:',x,y)
                # Your current position (S) has elevation a
                map[r][c] = PASSED
                break

    # change END to ord('z')+1
    for r in range(0,height):
        for c in range(0, width):
            if map[r][c] == END:
                print('End:',c,r)
                END = ord('z')+1
                map[r][c] = END                
                break

    steps = 0
    moves = []

    # start S
    moves.append(Move(x,y,ord('a')))

    # PART 2
    if PART == 2:
        for r in range(0,height):
            for c in range(0, width):
                if map[r][c] == ord('a'):
                    moves.append(Move(c,r,ord('a')))
                    map[r][c] = PASSED
                    print('Start:',c,r)

    finish = False
    while finish == False:
    #while steps <= 1:
        steps += 1
        #for mv in moves:
        #    print(mv)
        moves_count = len(moves)
        for m in range(moves_count):
            move = moves[m]
            #for l in map:
            #    print(l)

            if move.enabled == True:
                x = move.x
                y = move.y
                elevation = move.elevation
                # moving up (^)
                if y > 0:
                    if map[y-1][x] == END and elevation == ord('z'):
                        finish = True
                    elif (map[y-1][x]-elevation) <= 1:
                        moves.append(Move(x,y-1,map[y-1][x]))
                        map[y-1][x] = PASSED
                # down (v)
                if y < height-1:
                    if map[y+1][x] == END and elevation == ord('z'):
                        finish = True
                    elif (map[y+1][x]-elevation) <= 1:
                        moves.append(Move(x,y+1,map[y+1][x]))
                        map[y+1][x] = PASSED
                # left (<)
                if x > 0:
                    if map[y][x-1] == END and elevation == ord('z'):
                        finish = True
                    elif (map[y][x-1]-elevation) <= 1:
                        moves.append(Move(x-1,y,map[y][x-1]))
                        map[y][x-1] = PASSED
                # right (>)
                if x < width-1:
                    if map[y][x+1] == END and elevation == ord('z'):
                        finish = True
                    elif (map[y][x+1]-elevation) <= 1:
                        moves.append(Move(x+1,y,map[y][x+1]))
                        map[y][x+1] = PASSED

            move.disable()
        print("current step:",steps)
        #print_map(map)

    print_map(map)

    print("steps:",steps)


def print_map(map):

    for l in map:
        for c in l:
            if c == PASSED:
                print('.', end="")
            else:
                print(chr(c), end="")
        print()        

class Move:

    def __init__(self, x, y, elevation):
        self.x = x
        self.y = y
        self.elevation = elevation
        self.enabled = True

    def disable(self):
        self.enabled = False
    
    def __str__(self):
        return str(self.x)+","+str(self.y)+","+str(self.enabled)

if __name__ == "__main__":
    main()
