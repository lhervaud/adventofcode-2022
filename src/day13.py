# What is the sum of the indices of those pairs?

# PART 2

from sys import stdin

PART = 1
list1 = []
list2 = []

def main():

    line1 = ""
    line2 = ""
    indice = 1
    sum = 0

    # init map
    for line in stdin:
        input = line.rstrip()
        if input:
            if line1 == "":
                line1 = input
            else:
                line2 = input
        else:
            line1 = ""
            line2 = ""
            indice += 1

        if line1 != "" and line2 != "":
            result = run(line1,line2,indice)
            if result > 0:
                print("---INDICE---:",result)
                sum += result

    print("sum:",sum)


def run(line1,line2,indice):

    exec(f'list1 = {line1}', globals())
    exec(f'list2 = {line2}', globals())

    print(list1)
    print(list2)
    
    if compare(list1,list2) == True:
        return indice
    else:
        print("KO")
        return 0


def compare(list1,list2):
    stop = False
    order = True
    end = len(list1)
    if len(list1) == 0:
        order = True
        stop = True
    if len(list2) < end:
        order = False
        stop = True
    currentl = 0

    while stop == False:
        #print(currentl)
        l1 = list1[currentl]
        l2 = list2[currentl]
        # compare
        if type(l1) == type(l2):
            if type(l1) is int:
                if l1 > l2:
                    order = False
                    stop = True
            else:
                order = compare(l1,l2)
                if order == False:
                    stop = True
        else:
            if type(l1) is int:
                v = l1
                l1 = []
                l1.append(v)
            if type(l2) is int:
                v = l2
                l2 = []
                l2.append(v)
            #print(l1)
            #print(l2)
            if len(l1) == 0:
                stop = True
            elif len(l2) == 0:
                #order = False
                stop = True
            else:
                if type(l1[0]) != type(l2[0]):
                    order = False
                    stop = True
                elif l1[0] > l2[0]:
                    order = False
                    stop = True
            
        currentl += 1
        if currentl >= end:
            stop = True

    return order


if __name__ == "__main__":
    main()
