# How many units of sand come to rest before sand starts flowing into the abyss below?

# PART 2
# How many units of sand come to rest?

from sys import stdin

PART = 2
START = (500,0)
ROCK = '#'
SAND = 'o'
AIR = '.'

def main():

    puzzle = []
    map = []
    x_min = START[0]
    x_max = START[0]
    y_max = 0

    # init puzzle
    for line in stdin:
        input = line.rstrip()
        if input:
            path = []
            coords = input.split(" -> ")
            for coord in coords:
                c = coord.split(',')
                x = int(c[0])
                if x < x_min:
                    x_min = x
                if x > x_max:
                    x_max = x
                y = int(c[1])
                if y > y_max:
                    y_max = y
                path.append((x,y))
            puzzle.append(path)

    if PART == 2:
        # assume the floor is an infinite horizontal line with a y coordinate equal to two plus the highest y coordinate of any point in your scan
        x_min -= 300
        x_max += 300

    # init map
    for i in range(y_max+1):        
        map.append(list(AIR * (x_max+1)))
    map[START[1]][START[0]] = '+'
    if PART == 2:
        # assume the floor is an infinite horizontal line with a y coordinate equal to two plus the highest y coordinate of any point in your scan
        y_max += 2
        map.append(list(AIR * (x_max+1)))
        map.append(list(ROCK * (x_max+1)))

    # build map according to puzzle input
    for path in puzzle:
        for i in range(len(path)-1):
            coord1 = path[i]
            coord2 = path[i+1]
            x_step = 1
            y_step = 1
            if coord1[0] > coord2[0]:
                x_step = -1
            if coord1[1] > coord2[1]:
                y_step = -1
            for x in range(coord1[0], coord2[0]+x_step, x_step):               
                for y in range(coord1[1], coord2[1]+y_step, y_step):
                    map[y][x] = ROCK   

    print_map(map, x_min, x_max)

    print(run_sand(map, x_min, x_max, y_max))
    print_map(map, x_min, x_max)

def run_sand(map, x_min, x_max, y_max):

    void = False
    sand_count = 0

    while void == False:
        # one unit of sand
        sand_count += 1
        print("------------------------- sand_count:",sand_count)
        x = START[0]
        y = START[1]
        move = False
        # bottom
        while void == False and (map[y][x] == AIR or map[y][x] == '+'):
            y += 1
            if y > y_max:
                void = True
                print("void = True")
            if void == False and (map[y][x] == ROCK or map[y][x] == SAND):
                # down left
                if map[y][x-1] == AIR:
                    x -= 1
                else:
                    # down right
                    if map[y][x+1] == AIR:
                        x += 1
                    else:                    
                        # stop
                        map[y-1][x] = SAND
            move = True
            #print(x,y)

        if PART == 2 and move == False:
            void = True
        
        #print_map(map, x_min, x_max)

        if sand_count >= 100000:
            void = True

    return(sand_count-1)


def print_map(map, x_min, x_max):
    for line_map in map:
        print(''.join(line_map[x_min:x_max+1]))

if __name__ == "__main__":
    main()
