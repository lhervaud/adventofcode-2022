import sys
import pandas as pd

d = {}
# A for Rock, B for Paper, and C for Scissors
# X for Rock, Y for Paper, and Z for Scissors
# 1 for Rock, 2 for Paper, and 3 for Scissors
# 0 if you lost, 3 if the round was a draw, and 6 if you won
d["A X"] = 1 + 3
d["A Y"] = 2 + 6
d["A Z"] = 3 + 0
d["B X"] = 1 + 0
d["B Y"] = 2 + 3
d["B Z"] = 3 + 6
d["C X"] = 1 + 6
d["C Y"] = 2 + 0
d["C Z"] = 3 + 3

rows = []

for line in sys.stdin:
    combi = line.rstrip()
    if combi in d:
        rows.append([combi, d[combi]])
    else:
        print("key not exist:" + combi)

#print(rows)

df = pd.DataFrame(rows)
#print(df)
print(df[1].sum())
