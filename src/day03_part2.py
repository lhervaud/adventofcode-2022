# Find the item type that corresponds to the badges of each three-Elf group. What is the sum of the priorities of those item types?

from sys import stdin
from pandas import DataFrame as df
import numpy as np

def main():

    rows = []
    inters = []

    i = 0
    for line in stdin:
        input = list(line.rstrip())
        #print(input)
        rows.append(input)
        i += 1
        if i == 3:
            lin = np.intersect1d([rows[0]], rows[1])
            #print(lin)
            #print(rows[2])
            lin2 = np.intersect1d(lin, rows[2])[0]
            inters.append(get_letter_value(lin2))
            rows.clear()
            i = 0

    print(inters)

    df_score = df(inters)

    print(df_score[0].sum())


#Lowercase item types a through z have priorities 1 through 26.
#Uppercase item types A through Z have priorities 27 through 52.
def get_letter_value(c):
    if (c >= 'a' and c <= 'z'):
        return ord(c)-96
    elif (c >= 'A' and c <= 'Z'):
        return ord(c)-38
    else:
        return 0

if __name__ == "__main__":
    main()
